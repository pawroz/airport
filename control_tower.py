from collections import Counter
import time
from typing import Dict, List, Tuple
from space import Space
from plane import Plane
from utils import as_list
import threading


class ControlTower:
    def __init__(self, space: Space):
        self.space: Space = space
        self.lock = threading.Lock()

    def check_free_coords(self, coords: list) -> bool:
        if coords in self.space.planned_flight.values():
            return False
        return True

    def check_for_collision(self, current_flight_number: int) -> bool:
        """Checking for collisions in space. Compares coordinates within a range of 10 units to other planes.
        Return True if there is no collision."""
        try:
            self.lock.acquire()
            fligts_without_current_flight = {
                k: v for k, v in self.space.planned_flight.items() if k != current_flight_number
            }
            current_flight_coords = self.space.planned_flight[current_flight_number]
            print("current_flight_coords ", current_flight_coords)
            collision_zone1 = list(
                map(lambda x: x + 10, current_flight_coords))
            collision_zone2 = list(
                map(lambda x: x - 10, current_flight_coords))
            print("CL1", collision_zone1)
            print("CL2", collision_zone2)
            for k, v in fligts_without_current_flight.items():
                for item1, item2 in zip(collision_zone1, v):
                    if abs(item1 - item2) < 10:
                        print("pierwsza różnica mniejsza od 10")
                        return False
                for item1, item2 in zip(collision_zone2, v):
                    if abs(item1 - item2) < 10:
                        print("druga różnica mniejsza od 10")
                        return False
            return True
        except Exception as e:
            print(e)
        finally:
            self.lock.release()

    def plane_has_landed(self, flight_number: int, airstrip: list) -> bool:
        """Checks the plane's coordinates if coords agree with the landing strip coordinates returns True"""
        self.flights_id = list(self.flights_id)
        if self.space.planned_flight[flight_number] == airstrip:
            return True
        return False

    def start_flight(self, plane_id: int) -> bool:
        """Randomizes coords, 
        checking if coords are free,
        added coords to planned_flights return True if there is no collision in space.
        """
        coords = self.space.randomizes_coordinates()
        if self.check_free_coords(coords):
            self.space.add_flight(plane_id, coords)
            if self.check_for_collision(plane_id):
                return True
        return False

    def coordinate_equality(self, *args):
        pass

    def shortest_latitude_distance_to_specific_area(
        self, plane_coord: List[int], specific_area_coord: Tuple[int]
    ) -> bool:
        """Calculates the shortest distance of latitude to a given area.
        Subtracts or adds value to planes coord(x,_,_) depending on where it is closer to.
        """
        specific_area_coord = as_list(specific_area_coord)
        if plane_coord[0] in specific_area_coord:
            return True
        else:
            mini = abs(plane_coord[0] - specific_area_coord[0])
            mini2 = abs(plane_coord[0] - specific_area_coord[-1])
            if mini < mini2:
                print("dodawaj")
                plane_coord[0] += 1
                return False
            else:
                print("odejmij")
                plane_coord[0] -= 1
                return False

    def shortest_longitude_distance_to_specific_area(self, plane_coord: List[int], specific_area_coord: Tuple[int]) -> bool:
        """
        Calculates the shortest distance of longitude to a given area.
        Subtracts or adds value to planes coord(_,y,_) depending on where it is closer to.
        """
        specific_area_coord = as_list(specific_area_coord)
        if plane_coord[1] in specific_area_coord:
            return True
        else:
            mini = abs(plane_coord[1] - specific_area_coord[0])
            mini2 = abs(plane_coord[1] - specific_area_coord[-1])
            if mini < mini2:
                print("dodawaj")
                plane_coord[1] += 1
                return False
            else:
                print("odejmij")
                plane_coord[1] -= 1
                return False

    def shortest_altitude_distance_to_specific_area(self, plane_coord: List[int], specific_area_coord: Tuple[int]) -> bool:
        """
        Calculates the shortest distance of longitude to a given area.
        Subtracts or adds value to planes coord(_,_,z) depending on where it is closer to.
        """
        specific_area_coord = as_list(specific_area_coord)
        if plane_coord[2] in specific_area_coord:
            return True
        else:
            mini = abs(plane_coord[2] - specific_area_coord[0])
            mini2 = abs(plane_coord[2] - specific_area_coord[-1])
            if mini < mini2:
                print("dodawaj")
                plane_coord[2] += 1
                return False
            else:
                print("odejmij")
                plane_coord[2] -= 1
                return False


""" Experimental zone """

# space = Space()
# control_tower = ControlTower(space)
# for i in range(1, 11):
#     plane = Plane()
#     while control_tower.start_flight(plane.id):
#         current_position = space.planned_flight[plane.id]
#         plane.set_current_position(current_position)
#         break
#     print("probujemy jeszcze raz")
# print(space.air_tunnel()[1])
# print(space.planned_flight[5])
# time.sleep(3)
# while not (control_tower.shortest_longitude_distance_to_specific_area(
#     space.planned_flight[5], space.air_tunnel()[1]
# ) and control_tower.shortest_latitude_distance_to_specific_area(space.planned_flight[5], space.air_tunnel()[0])):
#     print(space.planned_flight[5])
# print(space.planned_flight)

# control_tower.shortest_latitude_distance_to_specific_area(space.planned_flight[5], space.air_tunnel()[0])
# 3000
# [3898, 4759, 3179] [494, 4438, 437]-ok 999-ok 823-ok 2645-ok
# dupa = as_list(space.air_tunnel()[0])
# print(dupa[0])

""" End experimental zone"""


""" TODO:
    -- 15.01 --
    1. Algorytm - wyznaczanie różnych ścieżek w celu uniknięcia kolizji samolotów w przestrzeni.

    -- 06.11 --
    1. W klasie Plane - mozliwosc poruszania sie samolotow - samolot zmienia lokalizacje szerokosci,
        dlugosci lub wysokosci w zalezności od tego czy wieza mu pozwoli.(if check_distance: break, else porusza sie inaczej)
    2. W klasie Control_tower - sprawdzenie czy samolot nie jest za blisko innego samolotu jezeli nie jest to pozwala na ruch,
        jezeli jest to kraksa: check_distance -> boolean
        dodać te funkcje sprawdzania bliskości do draw_coordinates przy tworzenu samolotu również trzeba sprawdzić czy nie są za blisko
    3. W klasie Space - dorobić coordynaty wskazujące na odpowiedni tor do lądowania 

    -- 07.11 --
    PRZED PRZYSTĄPIENIEM NAPISAC LOGIKE PORUSZANIA SIE SAMOLOTU Z PUNKTU A DO PUNKTU B
    kolejka samolotów które lecą w dół
    obszar w ktorym samoloty sie poruszaja w kółko 
    obszar do ktorego leci samolot -> tam zatacza kółka -> czeka na swoją kolejkę -> potem leci korytarzem wyznaczonym do pasa startowego
    korytarz do bezpiecznego obszaru

    1. Stworzyć tunel który prowadzi do obszaru gdzie krążą samoloty.
    2. Zakolejkować samoloty w tunelu zgodnie z czasem ich wlotu w przestrzeń tunelu. 
    3. Stworzyć obszar w którym samoloty krążą.
    4. Stworzyć koleję w obszarze oczekiwania -> pierwszy samolot, który dotarł przystąpi do lądowania również pierwszy FIFO.
    5. Stworzyć korytarz prowadzący do lądowania.

    -- 28.11 --
    1. Stworzyć server który będzie zarządzał kilkoma clientami i doprowadzał je do space.waiting_spot()[0]

    -- 07.12 -- 
    (SERVER <-> CLIENT)
    1. Server rozpoczyna działanie z zainicjowanym control_tower
    2. Każde połączenie clienta do serwera wywołuje control_tower.start_flight(plane.id)
        while control_tower.start_flight(plane.id):
            current_position = space.planned_flight[plane.id]
            plane.set_current_position(current_position)
            break
        print("probujemy jeszcze raz")
    3. Server czeka na nowe połączenie, gdy się połączy Client to prowadzi go do wyznaczonej strefy.

    -- 13.12 --
    1. Przemyśleć czy: Server nadaje id samolotom w metodzie add_flight i wysyła go do clienta, a on przypisuje danemu samolotowi to id
    """


"""Znalezienie najkrotszej sciezki szerokosci geograficznej do danej strefy. Przykład użycia."""

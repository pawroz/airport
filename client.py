from concurrent.futures import ThreadPoolExecutor
import socket
import time
import itertools
import json
from data_preparation import prepare_command_data
from typing import Dict, List, Tuple
from plane import Plane

HEADER = 1024
PORT = 5000
FORMAT = "utf-8"
DISCONNECT_MESSAGE = "!DISCONNECT"
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)


def send_planes_data(plane, connection) -> None:
    """Send initial plane data to server."""
    connection.send(json.dumps({"Samolot numer": plane.id, "ilosc paliwa": plane.fuel,
                    "coordy": plane.coords}).encode(FORMAT))  # sending information
    connection.send(
        json.dumps({"coordy": plane.coords,
                    "plane_id": plane.id}).encode(FORMAT)
    )


start_time = time.perf_counter()


def start(plane):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as c:
        c.connect(ADDR)
        server_message_json = json.loads(c.recv(1024).decode(FORMAT))
        print(server_message_json.get('data'))
        send_planes_data(plane, c)
        while True:
            server_message_json = json.loads(c.recv(1024).decode(FORMAT))
            print('chuj')
            if server_message_json.get('action') == 'INFO':
                print(server_message_json.get('data'))

        # c.send(
        #     f"Samolot numer: {plane.id} ilosc paliwa: {plane.fuel}, coordy: {plane.coords}".encode(
        #         FORMAT
        #     )
        # )  # sending information
        # c.send(
        #     json.dumps({"coordy": plane.coords,
        #             "plane_id": plane.id}).encode(FORMAT)
        # )  # sending plane data

        # # reveiving new coords from server
        # new_coords = json.loads(c.recv(1024).decode(FORMAT))
        # print(f"[FROM SERVER] {new_coords}")
        # plane.set_current_position(new_coords["plane_coords"])
        # print("NOWE KORDYNATY", plane.coords)
        # print("[FROM SERVER] ", c.recv(1024).decode(FORMAT))


planes = [Plane() for _ in range(5)]

with ThreadPoolExecutor(max_workers=25) as executor:
    fetches = executor.map(start, planes)


end_time = time.perf_counter()
elapsed_time = end_time - start_time
print(f"Czas wykonania: {elapsed_time:.2f} sekund")

"""
TODO: funkcja ktora caly czas w bloku try except czeka na socket od serwera patrzy na klucz 
    z zapytania od serwera i w poprawny sposob obslugłuje zapytanie/odpowiada klientowi

"""

import socket
import threading
from control_tower import ControlTower
from space import Space
import json
import time
from data_preparation import prepare_command_data

HEADER = 1024
PORT = 5000
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = "utf-8"
DISCONNECT_MESSAGE = "!DISCONNECT"
space = Space()
control_tower = ControlTower(space)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)
endpoints = ['INFO']


def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")
    conn.send(json.dumps({"action": "INFO", "data": "You are connected to the server", "command": None}).encode(FORMAT))
    print("[FROM CLIENT] ", conn.recv(HEADER).decode(FORMAT))
    msg_from_client = conn.recv(HEADER).decode(FORMAT)
    print(msg_from_client)
    msg_dict = json.loads(msg_from_client)
    plane_id = msg_dict['plane_id']
    try:
        while control_tower.start_flight(plane_id):
            current_position = space.planned_flight[plane_id]
            time.sleep(5)
            conn.send(json.dumps({"action": "INFO", "data": {
                      "plane_coords": current_position}, "command": "Null"}).encode(FORMAT))
            break
        print(current_position)
        while not (control_tower.shortest_latitude_distance_to_specific_area(current_position, space.air_tunnel()[0]) and
                   control_tower.shortest_longitude_distance_to_specific_area(current_position, space.air_tunnel()[1]) and
                   control_tower.shortest_altitude_distance_to_specific_area(current_position, space.air_tunnel()[2])):
            print(f'{plane_id} {space.planned_flight[plane_id]}')
            conn.send(json.dumps({"action": "INFO", "data": {
                      "Plane ID: ": plane_id, "plane_coords": current_position}, "command": "Null"}).encode(FORMAT))
    except Exception as e:
        print(e)

    print('planned_flight', len(space.planned_flight))
    print('flights', (space.planned_flight))
    conn.send(json.dumps({"action": "INFO", "data": {
                      "space.planned_flight": space.planned_flight}, "command": None}).encode(FORMAT))
    conn.close()


def start():
    server.listen()
    print(f"[LISTENING] Server is listening on {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


print("[STARTING] Server is starting...")
start()

# with ThreadPoolExecutor(5) as executor:
#     fetches = executor.submit(handle_client)

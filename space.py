import random
from typing import Dict, List, Tuple


class Space:
    Air_tunnel_coords = Tuple[tuple, int, int]

    def __init__(self, latitude=0, longitude=0, altitude=10000):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.borders = ((0, 5000), (0, 10000))
        self.planned_flight = {}
        self.planes_amount = 0

    def randomizes_coordinates(self) -> list:
        """Losuje koordynaty. Zwraca koordynaty"""
        coords = []
        min_border_value = 100
        max_border_value = self.borders[0][1]
        for i in range(2):
            coord = random.randint(min_border_value, max_border_value)
            coords.append(coord)
        coords.append(random.randint(min_border_value, max_border_value))
        return coords

    def add_flight(
        self, plane_id: int, coords: list
    ):  # TODO: ma dodawac coordy do obiektu plane
        """Dodaje samolot do przestrzeni powietrznej na losowe coordy"""
        self.planes_amount += 1
        self.planned_flight[plane_id] = coords
        return self.planned_flight

    # TODO: Pasy do lądowania muszą być dwa
    def airstrip(self, latitude, longitude, altitude):
        """Landing strips for aircraft."""
        return [latitude, longitude, altitude]

    def check_free_coords(self, plane_coords: list) -> list:
        """
        Check free coords in planned flights.
        Returns coordinates if there aren't any yet in planned flights.
        :param plane_coords: list | planes coords
        :return plane_coords: list | planes coords
        """
        while plane_coords in self.planned_flight.values():
            plane_coords = self.randomizes_coordinates()
        return plane_coords

    def air_tunnel(self) -> Air_tunnel_coords:
        """Tunnel where planes can get to waiting spot."""
        latitude = (3000, 3001)
        longitude = tuple(x for x in range(3000, 6001))
        altitude = (5000, 5001)
        air_tunnel_coords = (latitude, longitude, altitude)
        return air_tunnel_coords

    def waiting_spot(self):
        """A waiting area where planes can wait in queue to arrive at the airport."""
        latitude = tuple(x for x in range(6000, 9001))
        longitude = tuple(x for x in range(3000, 6001))
        # punkty obszaru poczekalni
        # corner_NW = tuple(x for x in range(3000, 9000))
        # corner_NE = tuple(x for x in range(6000, 9000))
        # corner_SW = tuple(x for x in range(3000, 6000))
        # corner_SE = tuple(x for x in range(6000, 6000))
        altitude = (5000, 5001)
        waiting_spot_coords = (latitude, longitude, altitude)
        return waiting_spot_coords


# space = Space()
# print(space.waiting_spot()[0])
# space.add_flight(1, [3537, 260, 8153])
# space.add_flight(2, [2222, 2222, 2222])
# coords = space.check_free_coords([3537, 260, 8153])
# print(coords)

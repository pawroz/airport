from space import Space
import time
import itertools
from typing import Dict, List, Tuple

# space = Space()


class Plane:
    num_instances = 0
    plane_id = itertools.count(1)

    def __init__(self):
        self.id: int = next(Plane.plane_id)
        self.fuel = 3
        self.time = 10
        self.coords = None

        Plane.num_instances += 1

    def __str__(self):
        return f"Samolot numer: {self.id} ilosc paliwa: {self.fuel}"

    def end_of_fuel(self):
        self.fuel -= 1
        if self.fuel == 0:
            return True
        return False

    def set_current_position(self, new_position: List[int]):
        """Setting current position of plane."""
        self.coords = new_position

    def movement_longitude(self):
        """Zmienia pozycje coord co sekundę o 2 jednostki.
        Sprawdzic na stacku jak poruszac elementem aby dotarł do odpowiedniego miejsca."""
        self.coords[0] += 5
        return self.coords

    def movement_latitude(self):
        self.coords[1] += 5
        return self.coords

    def movement_altitude(self):
        self.coords[2] += 5
        return self.coords